﻿using System;

public class Puzzle3
{

	public static void Main()
	{
		(int start, int end)[] TimePeriods = { (30, 75), (0, 50), (60, 150) };
		Array.Sort<(int start, int end)>(TimePeriods, (i1, i2) => {return i1.start.CompareTo(i2.start);});
		foreach (var j in TimePeriods)
		{
			Console.WriteLine("the array of time intervals are" +j);
			
		}
		int min = 0;
		for (int i = 1; i < TimePeriods.Length; i++)
		{
			if (TimePeriods[i - 1].end > TimePeriods[i].start)
				min =min+ 1;
		}
		Console.WriteLine($"the minimum number of rooms required is : {min}");
	}
}